This is the accounting for DebConf19. It uses [ledger][], a command-line
double-entry accounting program.

[ledger]: https://www.ledger-cli.org/

## File description

Meta files
* `accounts.inc`: a list of all the accounts used
* `commodities.inc`: a description of the commodities used
* `forex.db`: exchange rates

Ledger files:
* `budget.ledger`: the conference budget
* `expenses.ledger`: all expenses should be tracked there
* `incomes.ledger`: all incomes should be tracked there
* `journal.ledger`: meta ledger file that includes other relevant files

Scripts:
* `wrapper`: a more sensible way to call `ledger`
* `ledger-fx-rates`: fetches exchange rates from the European Central Bank

## Invoices

All expenses should be backed by invoices and be kept in the `invoices`
directory. The invoices should:

* be classified in directories by accounts
* start by `yyyymmdd_`
* have a short but descriptive name

For example:

```
invoices
├── childcare
│   └── 20170812_childcare.pdf
│   └── 20170817_music_show.pdf
├── roomboard
│   └── accommodation
│       ├── 20170810_hotel_universel_first_deposit.jpg
│       ├── 20170816_hotel_balance.pdf
│       └── 20170821_red_cross_rental.pdf
└── videoteam
    ├── 20170614_kyles_ticket.pdf
    └── 20170901_DigitalOcean_invoice_152897007.pdf
```

## Wrapper script

The easiest way to use this and include currency conversion is to call
`./wrapper` instead of `ledger`, e.g.

    ./wrapper -X USD bal

The wrapper script passes a few sensible default command-line parameter and
updates the currency exchange rates via `ledger-fx-rates`.

## Final report


```
$ ./wrapper -X BRL bal
      BRL 421.963,07  assets
        BRL 5.414,71    ICTL
      BRL 395.338,79    SPI
       BRL 21.209,57    debian-france
      BRL 418.502,12  expenses
       BRL 13.310,00    bursaries:general bursaries
          BRL 232,90    content
       BRL 21.989,00    daytrip
          BRL 170,10    fees
        BRL 3.202,16    general
        BRL 5.188,07    graphic materials
        BRL 2.697,89      banner
        BRL 2.294,50      paper
          BRL 195,68      poster
        BRL 4.020,85    infra
        BRL 2.045,86    insurance
        BRL 3.442,01    local team
          BRL 835,05      food
        BRL 2.606,96      transportation
       BRL 39.192,96    party
        BRL 3.324,31      cheese and wine
       BRL 35.868,65      conference dinner
        BRL 2.300,00        bus
        BRL 1.417,16        drink
       BRL 32.151,49        food
      BRL 211.772,81    roomboard
      BRL 158.497,14      accommodation
        BRL 2.068,74        access point
      BRL 153.832,00        bedrooms
          BRL 840,00        cleaning
          BRL 722,50        coffee and water
          BRL 800,00        internet link
          BRL 233,90        vegan food
       BRL 11.885,00      bar beer
       BRL 41.390,67      food
       BRL 38.843,68        catering
        BRL 3.221,28          barbecue
          BRL 243,10          daytrip
       BRL 26.525,00          hotel
        BRL 8.854,30          ru
        BRL 2.546,99        food
       BRL 29.655,71    swag
        BRL 5.588,00      backpack
          BRL 437,80      boton
        BRL 4.453,51      drink cup
        BRL 3.695,00      lanyard
       BRL 14.533,40      t-shirt
          BRL 948,00      tag
          BRL 793,10    tax
        BRL 6.785,76    venue:staff
       BRL 74.104,16    video
        BRL 4.301,76      cable
        BRL 7.268,00      computer rental
        BRL 3.700,65      fiber
       BRL 13.643,95      general
       BRL 28.880,00      projector
       BRL 16.309,80      sound equipment
        BRL 2.596,67    visa
     BRL -840.465,19  incomes
      BRL -15.788,71    bar
      BRL -11.586,00      cash for ICTL
       BRL -4.202,71      credit card for ICTL
      BRL -21.060,00    daytrip:cash for ICTL
          BRL -74,00    general:cash for ICTL
      BRL -89.574,54    registration
       BRL -7.170,37      cash for ICTL
      BRL -82.404,17      paypal for SPI
     BRL -713.967,94    sponsors
     BRL -657.204,16      international
      BRL -50.071,02        bronze
     BRL -102.219,40        gold
     BRL -204.438,80        platinum
     BRL -281.103,35        silver
      BRL -19.371,59        supporter
      BRL -56.763,78      national
      BRL -18.900,00        bronze
         BRL -180,00        donations
      BRL -31.500,00        silver
       BRL -6.183,78        supporter
--------------------
                   0

$ ./wrapper -X USD bal
       USD 82,560.27  assets
        USD 1,059.43    ICTL
       USD 77,351.03    SPI
        USD 4,149.81    debian-france
       USD 81,883.11  expenses
        USD 2,604.20    bursaries:general bursaries
           USD 45.57    content
        USD 4,302.31    daytrip
           USD 33.28    fees
          USD 626.53    general
        USD 1,015.09    graphic materials
          USD 527.86      banner
          USD 448.94      paper
           USD 38.29      poster
          USD 786.71    infra
          USD 400.29    insurance
          USD 673.46    local team
          USD 163.38      food
          USD 510.07      transportation
        USD 7,668.40    party
          USD 650.43      cheese and wine
        USD 7,017.97      conference dinner
          USD 450.01        bus
          USD 277.28        drink
        USD 6,290.68        food
       USD 41,434.95    roomboard
       USD 31,011.17      accommodation
          USD 404.76        access point
       USD 30,098.40        bedrooms
          USD 164.35        cleaning
          USD 141.36        coffee and water
          USD 156.53        internet link
           USD 45.76        vegan food
        USD 2,325.39      bar beer
        USD 8,098.40      food
        USD 7,600.06        catering
          USD 630.27          barbecue
           USD 47.56          daytrip
        USD 5,189.82          hotel
        USD 1,732.41          ru
          USD 498.34        food
        USD 5,802.36    swag
        USD 1,093.33      backpack
           USD 85.66      boton
          USD 871.36      drink cup
          USD 722.95      lanyard
        USD 2,843.57      t-shirt
          USD 185.48      tag
          USD 155.18    tax
        USD 1,327.69    venue:staff
       USD 14,499.04    video
          USD 841.67      cable
        USD 1,422.04      computer rental
          USD 724.06      fiber
        USD 2,669.54      general
        USD 5,650.59      projector
        USD 3,191.14      sound equipment
          USD 508.06    visa
     USD -164,443.38  incomes
       USD -3,089.18    bar
       USD -2,266.89      cash for ICTL
         USD -822.29      credit card for ICTL
       USD -4,120.55    daytrip:cash for ICTL
          USD -14.48    general:cash for ICTL
      USD -17,525.94    registration
       USD -1,402.94      cash for ICTL
      USD -16,123.00      paypal for SPI
     USD -139,693.24    sponsors
     USD -128,586.97      international
       USD -9,796.77        bronze
      USD -20,000.00        gold
      USD -40,000.00        platinum
      USD -55,000.00        silver
       USD -3,790.20        supporter
      USD -11,106.26      national
       USD -3,697.93        bronze
          USD -35.22        donations
       USD -6,163.21        silver
       USD -1,209.90        supporter
--------------------
                   0
```