# E-mail about DC19 logo chosen

## Portuguese

Assunto: DebConf19 tem uma logo

A organização da DebConf19 - Conferência Mundial de Desenvolvedores(as) do Projeto Debian [1], que acontecerá em Curitiba em julho de 2019 está orgulhosa em anúnciar a logo vencedora do concurso que recebeu propostas para a escolha da imagem que identificará o evento.

Recebemos 11 propostas de logos [2], algumas fazem alusão a Curitiba e outras ao Brasil. Depois foi feita uma votação pública para qualquer pessoa votar nas suas três logos preferidas. Foram computados os votos e o resultado final [3] indicou que a logo vencedora [4] foi a elaborada pelo brasileiro Ramon Mulin que combina a palavra DebConf19 com um tamanduá-bandeira [5].

O tamanduá-bandeira é uma das quatro maiores espécies de tamanduás encontradas na América Central e na América do Sul, e é uma espécie em perigo de extinção. É facilmente reconhecido pelo longo nariz e pela língua estendida, características que facilitam se alimentar de formigas e cupins, e o animal é um dos símbolos do Brasil.

Agradecemos a cada pessoa que disponibilizou seu tempo para elaborar e enviar sua proposta, e as pessoas que gentilmente participaram do processo de votação. Essas iniciatiavas fazem com que a organização da DebConf19 se sinta confiante que a comunidade irá apoiar e ajudar na realização de um grande evento no Brasil!

[1] https://www.debconf.org
[2] https://wiki.debconf.org/wiki/DebConf19/Artwork/LogoProposals
[3] https://salsa.debian.org/debconf-team/public/data/dc19/blob/master/artwork/logos/proposals/resultado-concurso-logo-debconf19.pdf
[4] https://wiki.debconf.org/wiki/DebConf19/Artwork
[5] https://pt.wikipedia.org/wiki/Tamandu%C3%A1-bandeira

## English

Subject: DebConf19 has a logo

The DebConf19 organization team is proud to announce the logo contest winner, DC19 will take place in Curitiba on July 2019.

We received 11 logos proposals [2], some of them referencing Curitiba and others Brazil. After that, there was a public poll where anyone could vote in their three favorite logo, then the votes were counted and the final result [3] gave us the winner logo [4], it was made by our fellow Brazilian Ramon Mulin and it combined the word DebConf with a "tamanduá-bandeira" [5].

The "Tamanduá-bandeira" (Banner Ant-eater) is one of the four biggest species of anteaters finded on Central America and South America, and is a species in danger of extintion. It's easily recognized by its long nose and extended tongue, this helps Tamanduá-bandeira on its diet, that consists of ants and termites, and also the animal is one of the brazilian symbols.

We thank each person who made their time available to make and submit their proposal, and the people who gently participated in the voting process. These initiatives make the DebConf19 organization feel confident that the community will support and help in holding a great event in Brazil!

[1] https://www.debconf.org
[2] https://wiki.debconf.org/wiki/DebConf19/Artwork/LogoProposals
[3] https://salsa.debian.org/debconf-team/public/data/dc19/blob/master/artwork/logos/proposals/resultado-concurso-logo-debconf19.pdf
[4] https://wiki.debconf.org/wiki/DebConf19/Artwork
[5] https://en.wikipedia.org/wiki/Giant_anteater
