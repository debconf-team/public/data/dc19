# E-mail about poll to choose DC19 logo

## Portuguese

Assunto: Concurso para escolha da logo da DC19 até 3 de junho

Olá,

Agora você pode ajudar a escolher a logo da DebConf19 votando até este domingo (03/06) nas propostas de sua preferência.

Acesse:
http://pesquisa.softwarelivre.org/index.php/585891


## English

Subject: Poll to choose the DC19 logo until june 3rd

Hi,

You can help to choose the DebConf19 logo voting in our poll until this sunday (06-03).

http://pesquisa.softwarelivre.org/index.php/585891?lang=en

Best regards,

