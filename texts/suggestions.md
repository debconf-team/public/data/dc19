This is copy from:

https://pad.riseup.net/p/DC19Suggestions-keep


DebConf19

If you have any suggestion or question or concern, please insert below. We will be happy to read all and will promise try to implement all.

# 1) Suggestions:

## SWAG

1. long sleeved shirts (it will be winter!) +1
1. an option in the registration form to say that you'd like additional T-shirt(s), and give the size(s). All lot of people want to have 2, or bring one back for their partner / children, an that allows to order the right amount (probably needs to announce the price of the additional shirts, though)
1. seems like a lot of people would like to buy Debian swag during DebConf, probably the orga team will have too much work to do this, but it might be delegated to someone?
1. announce the option for sticker exchange (table) in advance (so people can prepare/bring enough of them to share/exchange)
1. ask your sponsors if they have goodies for us, we usualy have pens, notebook, stickers, keychains... directly in the swagbag

## FOOD

1. instead of counting the vegetarians and providing the exact number of veggie meals, the rest being meat, let's do the reverse! All food veggie, and the right amount of meat for meat-eaters (they can eat veggie if there's need, but we can't eat meat when some of them found the veggie meal tempting and ate it all) +1+1+1+1+1 -1 Being a meat-eater, I would really love to see more (in quantity and quality) appealing vegan and/or vegetarian meals - I'm cutting down on my meat intake and the strict distinction at DebConf is not helping there. -1 this may end with only "side dish" for the vegetarians.
1. vegetarians & vegans should have a replacement for every meat dish like : chili con carne/chili con soja ; rice salad with tuna/rice salad with chickpea ; red beans with pork/simple red beans ; pasta with ham and cheese/pasta with tomato sauce ; grilled fish/grilled tofu... With a good organisation it's not a lot of more work for the kitchen- I can help for menu suggestions if needed (CathyCheeseMaster).
1. in Taiwan, the fresh fruits for dessert were a big success, probably a good idea for next year too :) And Brazilian fruits are *AWESOME* +1 +1 +1 +1
1. It could be nice if we have a lot of fruits, such as mango and banana
1. This has been already (partly) implemented in DebConf18, but please make sure to label food allergens. This is really crucial that people with allergies can trust this information because the consequences are really serious (ER room…). (in the EU this is the law, and (especially for Restaurants) there is a standardized scheme how to label food with the 14 major allergenes.(e.g. see http://goo.gl/mLXnRm for an example from an restaurant's menu;
1. another possibility could be to provide the complete ingredients list and list the allegens in "bold" typeface.)
1. If you plan to have a buffet, make sure to sensitize antendants not to introduce contaminaztinos to the next plate on the buffet by using their silverware or the silverware of the next plate. or better arrange the food so that contamination cannot happen.

## VENUE

1. More/better signs to Hacklabs and talking rooms (and possibly toilets, free phones, water, etc.).
1. If cold/heat could be an issue, add few smaller hacklabs (hot hacklab, cool&cold hacklab, +quiet variants)

## COFFEE / TEA

1. If we make our own coffee on site, put a note on how to make more coffee when it's empty with a step by step explaination and all the things needed (coffee, filters, water...) nearby.
1. Same for tea and herbal tea

## BEER

1. If there is a beer-bar, not in a hacklab & not in a place with carpet floor.
2. Near the karaoke should be fine :-p 
3. We shoud have reusable glasses (like in DC13 switzerland)

## CHEESE & WINE

1. send a reminder to people to bring their local specialties a week before Debcamp, then a few days before DebConf, also mention which import restrictions apply +1+1 (I'll do=CathyCheeseMaster)
1. also ask people to register their cheese & country in the wiki (so we can print nice labels)(I'll do)
1. ask for volunteer in the same reminder and ask them, if possible, to bring knife & corkscrew (I'll do)
1. not in dark, please... +1 +1(or bring a projector from the venue)
1. not in the hot & not in the cold (may be fresh at night in augustus in Curitiba)
1. more separated tables for the food with space to walk between them to avoid overcrowding  (eg like at dc13 (about 6 tables in a circle) or dc17 (tables on two levels))
1. No Durian at the desserts table pleaaase!
1. FRIDGE : a fridge must be available to attendees from day 1 of Debcamp, (maybe at the Hotel ?) so they could bring their cheese. Paper & pen should be available with a note to remind to label their cheese before put it in the fridge and register in the wiki.

## DAYTRIP

1. the day trip is made to know each other outside the computer work. In recent years it has been divided into teams because it's difficult to do something with such a big group. But if it's possible, that would be better. Well, it'll be too cold for the beach, but a picknick in a park or in a natural area, with social games (sport, walks, beach games, juggling...) ; or renting a beer train, or a boat trip...
1. I personally  like the fact that it's small groups and that being separated from my usual friends makes me speak to new people :)
1. We could have both: small groups and common lunch or dinner (maybe more than just the eating part).
1. I liked in DC9 a group visit of the city (one evening) [Note: city center was in walking distance]

## TRANSPORT

1. provide the phone numbers of english-speaking taxis ahead of time +1
1. If possible, try to find a way to make easy to rent bicycle
1. If possible, provide more pointers to local bus stop timetable. It is a bit hard to collect information about local bus for foreigner +1

## VIDEO

1. I had difficulties having only VGA output, that the video team considers "legacy". I understand it's hard to support everything, but maybe warn in advance that it might not work and to borrow someone else's computer, so that all people in the same situation can prepare in advance (there are still a number of people with X200 like me, or similar generations of computers)
1. This is (and has been for a few years) documented in the "Advice for Presenters".
1. But give instruction better
1. How's about sending that information in a mail when a talk is registered (I did not get a mail on registration of the (ad hoc) talk at least). This could also include other important (video team) hints..

## FRONTDESK

1. please always provide at least one print out schedule at the entrance so that we don't have to look at phones and computers for that [provide to people, or just one large fix schedule?]
1. please have locals actively present on the frontdesk. It has been too chaotic and centralized in a small amount of people for the last debconfs. [with local locals]
1. If possible, please prepare card holder and meal ticket in advance for late arrival attendees during DebConf. When I've checked in, I could not get it both until next day.
1. more tools (scissors, pens, paper, sharpies)
1. free leftover fruits at front desk at dc18 where great
 
## PHOTO POLICY

1. See my emails (Ulrike) to debconf-team about this issue. Please provide a clear photo policy that is opt-in, not opt-out after the fact. Could be done using colored lanyards. Please also provide non photo zones in the hacklab. Making sure everybody has read the CoC did not seem to be enough in Taiwan. Please no opt-in policy.
1. Excellent suggestion! Also involves camera operators (no crowd shots), in particular don't shoot people's screens (!) from behind.
1. Please keep the possibility of having unrecorded BoFs and talks, like in Taiwan. This was possible at all DebConfs, so shouldn't be a big deal.

## BADGES

1. I'd like to see on the badges an officially printed out field called "Talk to me about …" → this will help especially newcomers to get in touch, but will also be useful for everybody else.
1. The double faced badge in DC18 was nice.+1

## NEWCOMERS

1. It would be nice if we can have a workshop in the debcamp to learn how to package. It could be an excellent opportunity for each team to attract newcomer.
1. How about "may I help you?" session with newcomers during debconf (not after event) → reinstate "New to DebConf" sessions like we had in CapeTown
1. have Debian Women lunch at some point at the beginning of the conference (see report & ideas here https://lists.debian.org/debian-women/2018/08/msg00002.html)
1. Schedule one introduction round (see thread here: https://lists.debian.org/debian-women/2018/08/msg00007.html)
 
## SCHEDULE

- more night owl friendly schedule with holes inbetween (eg for siesta)
   dinner could be at 9PM instead of 6PM  -1
   →→→→→ oh no, please don't do that! Let's instead try to stick to local customs? Like when do locals eat and sleep? +1 That's what we usually do. Caterers often insist on their usual food times.
 - allow GSoC and Outreachy students to present their work in a plenary session, at the beginning of the conference. (see also https://lists.debian.org/debian-women/2018/08/msg00002.html → entire thread) 
   This will help:
   - make their work known in Debian
   - help them connect with our community
   - stay involved in our community once GSoC/Outreachy is over

## Networking

1. check the Wifi coverage, ask local admins how many clients their wifi can handle
1. If the Wifi isn't sufficient and we will set up our own network, it would be great to have a map of the venue including already existing access points
1. frequent signs with wifi access information
1. only broadcast one SSID to speed up connectivity after relocating

## VARIOUS

1. Some people will arrive and will be already in Curitiba at least one week before DebCamp or will stay one more week after DebConf. Would be great if you as locals could provide some information what foreign guests could do or should do, what type of traveling you would suggest (car, bus, ...), where to go etc.
1. Also nice would be some information how public transport is working in Curitiba or nearby.
1. It's about 12 years ago I was last in Brazil, some local words I can remember but not that lot. ;) What about some small info inside the badge with some typical needed translations like numbers, greetings etc.?
1. saw this on the notes about ignoring negativity BoF Next year, remember to schedule a recorded "Lightning thanks" session.

# 2) Questions:

1. What is the situation for evening activities, like restaurants, bars/pubs/..., near the venue/hotels?
1. Network, especially ping time to Europe (for misc work)
1. Accessibility: How compatible is the whole venue (and travel to it) with using a wheelchair? Will speaker desks be accessible?

# 3) Concerns:

1. please rent some electric heaters for the hacklabs. An external patio heater would also be great for the outdoor hacklab.
Check with the venue that the power supply of hacklabs is enough including the heaters...
1. Essential that the hacklab, food & venue arrangements are engineered and located such as to reduce fragmentation. eg. definitely worth spending a little to ensure that attendees are generally in the same locations most of the time, otherwise evenings to promote collaboration and an atmosphere. It should be "easy" for folks to be around each other most of the time, and not require organisation, otherwise people will find their own stuff to do. +1
1. one centralized place for night activity avoids split up of people (dc17 bar, dc13 bar, dc18 bar)
1. Pretty often DebConf is not a very nice place for addicts to tobacco (aka. smokers).
   Sometimes it's been OK-ish, sometimes it's been awful.
   I personally know of one person who did not attend this year in great part due to the expected
   situation around the venue for smokers.
   It's of course a good thing to protect everyone else from our disgusting emanations,
   but still, it would be very sweet if even the poor damned souls we are could enjoy DebConf
   a bit more :)
   So here are my questions and personal wishes between square brackets:
       - How far from the main venue (where talks/BoFs are held) is the closest smoking area?
         [MUST: max 5 minutes walk; SHOULD: max 1 minute walk]
       - How far from the hotel hacklab (where one can drink alcohol) is the closest smoking area?
         [MUST: max 5 minutes walk; SHOULD: max 1 minute walk]
       - Is there Wi-Fi coverage in the aforementioned smoking areas? [MUST]
       - Is it feasible to bring electrical power to the aforementioned smoking areas? [MAY]
       - Are there benches / chairs in the smoking area? [MUST]
       - Is the smoking area usable if raining or much sun? [MUST]
1. ALWAYS ALWAYS GET IT IN WRITING, fsvo IT - contracts, services, venue commitments, etc.
1. It was not possible for me to register without creating a paypal account (seemed to depend on country). Paypal is such a deceitful company, it would be really great to find a better payment processor.
   
   Thanks a lot for organizing DebConf19,
   ♥
   -- 
   intrigeri <intrigeri@debian.org>


